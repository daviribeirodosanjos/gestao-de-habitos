import { useHistory } from "react-router";
import { Card } from "@material-ui/core";
import HomeIcon from "@material-ui/icons/Home";
import "./infoStyle.css";

const Info = () => {
  const history = useHistory();

  return (
    <div className="info">
      <h1 style={{ textAlign: "center" }}>Informações</h1>

      <div className="info-container about">
        <Card className="info-card-about">
          <span>
            <h2>Sobre nós</h2>
            <p>
              Franklin Percicotte <span>Product Onwer</span>
            </p>
            <p>
              Anderson Valério <span>Tech Lead</span>
            </p>
            <p>
              Graziela M. Kranz <span>Scrum Master</span>
            </p>
            <p>
              Alexandre Uglar <span>Quality Assurance</span>
            </p>
            <p>
              Davi Anjos <span>Quality Assurance</span>
            </p>
            <br />
            <p>
              Filipe Gutierry <span>Instrutor</span>
            </p>
            <p>
              Willian Brusch <span>Facilitador</span>
            </p>
            <p>
              Felipe S. Molina <span>Coach</span>
            </p>
          </span>
          <p>
            Q2 - 03/2021
            <span>
              &emsp;
              {
                <HomeIcon
                  fontSize="medium"
                  onClick={() => history.push("/home")}
                  className="info-iconHome"
                />
              }
            </span>
          </p>
          <a
            href="https://kenzie.com.br/"
            className="info-kenzie"
            target="blank"
          >
            Kenzie Academy
          </a>
        </Card>
      </div>
    </div>
  );
};

export default Info;
