import { useForm } from "react-hook-form";
import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
import { useToken } from "../../Providers/Token";
import { useUser } from "../../Providers/User";
import { useHabits } from "../../Providers/Habits";
import { TextField } from "@material-ui/core";
import { useEffect } from "react";
import "./formHabitCreate.css";

const FormHabitCreate = ({ setHabitCreate, setOpenNew }) => {
  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório"),
    category: yup.string().required("Campo obrigatório"),
    difficulty: yup.string().required("Campo obrigatório"),
    frequency: yup.string().required("Campo obrigatório"),
  });

  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(schema),
  });

  const { token } = useToken();
  const { user, loadUser } = useUser();
  const { habits, loadListHabits } = useHabits();

  useEffect(() => {
    loadUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [habits]);

  const handleForm = (_data) => {
    const data = {
      ..._data,
      user: user.id,
      achieved: false,
      how_much_achieved: 0,
    };
    const url = "https://kabit-api.herokuapp.com/habits/";
    axios
      .post(url, data, {
        headers: { Authorization: `Bearer ${JSON.parse(token)}` },
      })
      .then((response) => {
        loadListHabits();
        reset();
        setOpenNew(false);
      })
      .catch((error) => console.log(error));
  };

  return (
    <div className="habit-create-container">
      <h2>Novo hábito</h2>
      <form onSubmit={handleSubmit(handleForm)}>
        <TextField
          margin="normal"
          variant="outlined"
          label="Hábito"
          name="title"
          size="small"
          color="primary"
          inputRef={register}
          error={!!errors.title}
          helperText={errors.title?.message}
        />
        <TextField
          margin="normal"
          variant="outlined"
          label="Categoria"
          name="category"
          size="small"
          color="primary"
          inputRef={register}
          error={!!errors.category}
          helperText={errors.category?.message}
        />
        <TextField
          margin="normal"
          variant="outlined"
          label="Dificuldade"
          name="difficulty"
          size="small"
          color="primary"
          inputRef={register}
          error={!!errors.difficulty}
          helperText={errors.difficulty?.message}
        />
        <TextField
          margin="normal"
          variant="outlined"
          label="Frequência"
          name="frequency"
          size="small"
          color="primary"
          inputRef={register}
          error={!!errors.frequency}
          helperText={errors.frequency?.message}
        />
        <div className="habit-create-buttons">
          <button type="submit">
            <SaveIcon fontSize="large" />
          </button>
          <button type='button' onClick={() => setOpenNew(false)}>
            <ExitToAppIcon fontSize="large" />
          </button>
        </div>
      </form>
    </div>
  );
};

export default FormHabitCreate;
