import { useForm } from "react-hook-form";
import { useToken } from "../../Providers/Token";
import { useUser } from "../../Providers/User";
import { useHistory } from "react-router";
import { useState } from "react";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
import { createMuiTheme, styled } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import MuiAlert from "@material-ui/lab/Alert";
import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { Button, TextField } from "@material-ui/core";
import "./createGroup.css";

const Text = styled(TextField)({
  margin: "5px",
  width: "350px",
});

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const CreateGroup = ({ setOpenNew }) => {
  const [error, setError] = useState(false);
  const { loadUser } = useUser();
  const history = useHistory();
  const { token } = useToken();

  const schema = yup.object().shape({
    name: yup.string().required("Campo obrigatório"),
    description: yup.string().required("Campo obrigatório"),
    category: yup.string().required("Campo obrigatório"),
  });

  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(schema),
  });

  const subscribe = (id) => {
    const api = `https://kabit-api.herokuapp.com/groups/${id}/subscribe/`;
    axios
      .post(
        api,
        {},
        { headers: { Authorization: `Bearer ${JSON.parse(token)}` } }
      )
      .then(() => {
        loadUser();
        history.push("/");
      });
  };

  const handleForm = (data) => {
    const url = "https://kabit-api.herokuapp.com/groups/";
    axios
      .post(url, data, {
        headers: { Authorization: `Bearer ${JSON.parse(token)}` },
      })
      .then((response) => {
        subscribe(response.data.id);
        setOpenNew(false);
        reset();
      })
      .catch((error) => {
        if (error.response.status === 400) {
          setError("Este nome de usuário já existe!");
          reset();
        }
      });
  };

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#284B63",
      },
      secondary: {
        main: "#153243",
      },
      error: {
        main: "#FF220C",
      },
    },
  });

  const back = () => {
    setOpenNew(false);
  };

  return (
    <div className="create-group">
      <h1>Criar Grupo</h1>
      <br />
      <form onSubmit={handleSubmit(handleForm)}>
        <ThemeProvider theme={theme}>
          <Text
            margin="normal"
            variant="outlined"
            label="Nome"
            name="name"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.name}
            helperText={errors.name?.message}
          />
          <Text
            autoComplete="description"
            margin="normal"
            variant="outlined"
            label="Descrição"
            name="description"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.description}
            helperText={errors.description?.message}
          />
          <Text
            autoComplete="current-category"
            type="category"
            margin="normal"
            variant="outlined"
            label="Categoria"
            name="category"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.category}
            helperText={errors.category?.message}
          />
          <div className="buttons-group">
            <div>
              <Button type="submit" color="primary">
                <SaveIcon fontSize="large" />
              </Button>
              <Button color="primary" onClick={() => back()}>
                <ExitToAppIcon fontSize="large" />
              </Button>
            </div>
          </div>
        </ThemeProvider>
        {error && <Alert severity="warning">{error}</Alert>}
      </form>
    </div>
  );
};
export default CreateGroup;
