import { Card } from "@material-ui/core";
import { useHabits } from "../../Providers/Habits";

const ListViewHabits = () => {
  const { habits } = useHabits();

  return (
    <>
      {habits &&
        [...habits].map((elm, idx) => (
          <Card
            key={idx}
            style={{
              margin: "16px",
              padding: "16px 16px",
              border: "1px solid blue",
              width: `360px`,
            }}
          >
            <div> Titulo: {elm.title}</div>
            <div> Categoria: {elm.category}</div>
            <div> Frequencia: {elm.frequency}</div>
            <div> Dificuldade: {elm.difficulty}</div>
            <div style={{ display: "inline-block" }}> Andamento: &nbsp;</div>
            <div
              style={{
                display: "inline-block",
                width: `${elm.how_much_achieved}px`,
                height: "5px",
                border: "2px solid red",
                backgroundColor: "red",
              }}
            ></div>
            <br />
          </Card>
        ))}
    </>
  );
};

export default ListViewHabits;
