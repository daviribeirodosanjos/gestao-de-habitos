import { useHistory } from "react-router";
import { useToken } from "../../Providers/Token/index";
import { useUser } from "../../Providers/User";
import { useGroups } from "../../Providers/Groups";
import { useHabits } from "../../Providers/Habits";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import "./logout.css";

const Logout = () => {
  const history = useHistory();
  const { setToken } = useToken();
  const { setUser } = useUser();
  const { setGroups } = useGroups();
  const { setHabits } = useHabits();

  const setLogout = () => {
    setToken(false);
    setGroups(false);
    setUser(false);
    setHabits(false);
    history.push("/");
  };

  return (
    <button onClick={setLogout} className="logout-button">
      <ExitToAppIcon fontSize="large" />
    </button>
  );
};

export default Logout;
