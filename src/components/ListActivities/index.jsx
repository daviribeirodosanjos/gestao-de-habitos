import { useUser } from "../../Providers/User";
import { useEffect, useState } from "react";
import { useToken } from "../../Providers/Token";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import axios from "axios";

import { Button, TextField } from "@material-ui/core";

const ListActivities = () => {
  const { user, loadUser } = useUser();
  const { token } = useToken();

  const [filterForm, setFilterForm] = useState({
    title: "",
    realization_time: "",
    group: 0,
  });

  useEffect(() => {
    loadUser();
    // eslint-disable-next-line
  }, []);

  //Get activities
  const [listActivities, setListActivities] = useState();
  const [url] = useState("https://kabit-api.herokuapp.com/activities/");
  useEffect(() => {
    axios.get(url).then((response) => {
      setListActivities(response.data.results);
    });
  }, [filterForm, url]);

  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório"),
    date: yup
      .string()
      .required("Campo obrigatório")
      .matches(
        /(\d{4})[-./](\d{2})[-./](\d{2})/,
        "Tem que ser nesse formato aaaa-mm-dd"
      ),
    time: yup
      .string()
      .required("Campo obrigatório")
      .matches(
        /^([0-1]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/,
        "Tem que ser nesse formato hh:mm:ss"
      ),
  });

  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(schema),
  });

  const handleForm = (data) => {
    let value = `${data.date}T${data.time}Z`;

    setFilterForm(
      (filterForm.realization_time = value),
      (filterForm.title = data.title),
      (filterForm.group = user.group)
    );

    axios
      .post("https://kabit-api.herokuapp.com/activities/", filterForm, {
        headers: { Authorization: `Bearer ${JSON.parse(token)}` },
      })
      .then((response) => {
        reset();
      })
      .catch((error) => console.log(error));
  };

  return (
    <div>
      <h1>Lista Atividades</h1>
      <div>
        {listActivities &&
          listActivities.map((elm, index) => {
            return (
              <div className="Groups" key={index}>
                <h2>Titulo: {elm.title}</h2>
                <p>Grupo: {elm.group}</p>
                <p>Data de realizacao: {elm.realization_time}</p>
                <br />
              </div>
            );
          })}
      </div>
      {/* {listActivities.previous && <button onClick={() => setUrl(listActivities.)}>Anterior</button>} */}
      <br />
      <div>
        <h1> Criar Atividade</h1>
        <form onSubmit={handleSubmit(handleForm)}>
          <TextField
            margin="normal"
            variant="outlined"
            label="Atividade"
            name="title"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.title}
            helperText={errors.title?.message}
          />
          <br />
          <TextField
            margin="normal"
            variant="outlined"
            label="Data"
            name="date"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.date}
            helperText={errors.date?.message}
          />
          <br />
          <TextField
            margin="normal"
            variant="outlined"
            label="Horário"
            name="time"
            size="small"
            color="primary"
            inputRef={register}
            error={!!errors.time}
            helperText={errors.time?.message}
          />
          <br />
          <Button
            type="submit"
            variant="contained"
            color="primary"
            onClick={() => null}
          >
            Enviar
          </Button>
        </form>
      </div>
    </div>
  );
};

export default ListActivities;
