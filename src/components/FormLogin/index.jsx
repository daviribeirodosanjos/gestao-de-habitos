import { useForm } from "react-hook-form";
import "./formLogin.css";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import { useToken } from "../../Providers/Token";
import MuiAlert from "@material-ui/lab/Alert";
import axios from "axios";
import { Button, TextField } from "@material-ui/core";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const FormLogin = () => {
  const [error, setError] = useState(false);
  const { setToken } = useToken();

  const history = useHistory();

  const schema = yup.object().shape({
    username: yup.string().required("Campo obrigatório"),
    password: yup
      .string()
      .min(6, "Mínimo de 6 dígitos")
      .required("Campo obrigatório"),
  });

  const { register, handleSubmit, errors, reset } = useForm({
    resolver: yupResolver(schema),
  });

  const handleForm = (data) => {
    axios
      .post("https://kabit-api.herokuapp.com/sessions/", data)
      .then((response) => {
        localStorage.clear();
        setToken(JSON.stringify(response.data.access));
        reset();
        history.push("/home");
      })
      .catch((error) => {
        if (error.response.status === 401) {
          reset();
          setError("Usuário e/ou senha inválidos");
        }
      });
  };

  const sendToRegister = () => {
    history.push("/register");
  };

  // const handleCheck = () => {
  //   setKeepLogin(!keepLogin);
  // };

  return (
    <div className="login-container">
      <h2>Bem vindo(a) ao Virtuose</h2>
      <h4>Está na hora de não deixar para amanhã!</h4>
      <form onSubmit={handleSubmit(handleForm)}>
        <TextField
          autoComplete="user"
          margin="normal"
          variant="outlined"
          label="Username"
          name="username"
          size="small"
          color="primary"
          inputRef={register}
          error={!!errors.username}
          helperText={errors.username?.message}
        />

        <TextField
          autoComplete="current-password"
          type="password"
          margin="normal"
          variant="outlined"
          label="Senha"
          name="password"
          size="small"
          color="primary"
          inputRef={register}
          error={!!errors.password}
          helperText={errors.password?.message}
        />

        <div className="buttons-login">
          <Button type="submit" variant="contained" color="primary">
            Enviar
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={() => sendToRegister()}
          >
            Cadastrar
          </Button>
          {/* <FormControlLabel
            control={
              <Checkbox
                checked={keepLogin}
                onChange={handleCheck}
                name="checkedB"
                color="primary"
              />
            }
            label="Permanecer conectado"
          /> */}
        </div>
      </form>
      {error && (
        <Alert severity="warning" className="alert">
          {error}
        </Alert>
      )}
    </div>
  );
};

export default FormLogin;
