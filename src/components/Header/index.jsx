import { useState } from "react";
import { useHistory } from "react-router-dom";
import { useToken } from "../../Providers/Token";
import UserEdit from "../UserEdit";
import Logout from "../Logout";
import ModalCard from "../ModalCard";
import EqualizerIcon from "@material-ui/icons/Equalizer";
import PersonIcon from "@material-ui/icons/Person";
import "./header.css";

const Header = () => {
  const { token } = useToken();
  const history = useHistory();
  const [open, setOpen] = useState(false);

  return (
    <div className="header">
      <div className="header-container">
        <div className="logo">
          <figure>
            <EqualizerIcon className="icon" />
          </figure>
          <h2 onClick={() => history.push("/")}>Virtuose</h2>
        </div>
        <div>
          {JSON.parse(token) && (
            <div>
              <div className="logout">
                <button>
                  <PersonIcon onClick={() => setOpen(true)} fontSize="large" />
                </button>
                <Logout />
              </div>
            </div>
          )}
        </div>
      </div>
      {open && (
        <ModalCard open={open} setOpen={setOpen}>
          <UserEdit setOpen={setOpen} color="secondary" />
        </ModalCard>
      )}
    </div>
  );
};

export default Header;
