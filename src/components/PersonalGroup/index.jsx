import { useUser } from "../../Providers/User";
import axios from "axios";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import GroupGoalsList from "../GroupGoalsList";
import CardGroupActivities from "../CardGroupActivities";
import "./personalGroup.css";
import CreateGroup from "../CreateGroup";

const PersonalGroup = () => {
  const history = useHistory();
  const { user, loadUser, loadGroup } = useUser();
  const [group, setGroup] = useState();

  useEffect(() => {
    loadUser();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    user && loadGroup();
    // eslint-disable-next-line
  }, [user]);

  const getGroup = () => {
    axios
      .get(`https://kabit-api.herokuapp.com/groups/${user.group}/`)
      .then((response) => {
        setGroup(response.data);
      });
  };

  useEffect(() => {
    if (user) {
      getGroup();
    }
    // eslint-disable-next-line
  }, [user]);
  return (
    <>
      {group ? (
        <div className="personal-group-container">
          <div>
            <div className="group-data">
              <h1>Detalhes do seu grupo</h1>
              <h2>{group.name}</h2>
              <div>
                <div className="description">
                  <span>Descrição</span> <section>{group.description}</section>
                </div>
                <div className="category">
                  <span>Categoria</span> <section>{group.category}</section>
                </div>
              </div>
            </div>
            <div className="group-goals-container">
              <GroupGoalsList />
              <CardGroupActivities />
            </div>
          </div>
        </div>
      ) : (
        <div className="no-group-container personal-group-container">
          <div>
            <h2>Você ainda não tem um grupo!</h2>
            <p>
              Praticar em grupo é o melhor meio de driblar a procrastinação!
            </p>
            <button onClick={() => history.push("/group")}>Ver grupos</button>
            <p>Ou você pode criar um grupo pra chamar de seu!</p>
            <div className="card-create-group">
              <CreateGroup />
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default PersonalGroup;
