import { useState } from "react";
import { useUser } from "../../Providers/User";
import { useToken } from "../../Providers/Token";
import GroupGoalsEdit from "../GroupGoalsEdit";
import GroupGoalsCreate from "../GroupGoalsCreate";
import { Card, Checkbox } from "@material-ui/core";
import axios from "axios";
import AddCircleRoundedIcon from "@material-ui/icons/AddCircleRounded";
import DoneAllOutlinedIcon from "@material-ui/icons/DoneAllOutlined";
import MenuOpenOutlinedIcon from "@material-ui/icons/MenuOpenOutlined";
import ModalCard from "../ModalCard";
import "./cardCheckStyles.css";
import "../../styles/global";

const GroupGoalsList = () => {
  const { token } = useToken();
  const [openEdit, setOpenEdit] = useState(false);
  const [openNew, setOpenNew] = useState(false);
  const [checked, setChecked] = useState([
    {
      value: false,
    },
  ]);

  const { groupUser, setGroupUser, loadGroup } = useUser();
  const [goalsEdit, setGoalsEdit] = useState("");
  const [goalsCreate, setGoalsCreate] = useState(false);
  const showGoalsDetails = (elm) => {
    setGoalsEdit(elm);
    setOpenEdit(true);
  };

  const newGoals = () => {
    setOpenNew(true);
    setGoalsCreate(true);
  };

  const handleChange = (event) => {
    setChecked([...checked, { value: true }]);
    const url = `https://kabit-api.herokuapp.com/goals/${event.target.value}/`;
    const data = { achieved: true, how_much_achieved: 100 };
    const patchGoal = (url, data) => {
      axios
        .patch(url, data, {
          headers: { Authorization: `Bearer ${JSON.parse(token)}` },
        })
        .then((response) => {
          loadGroup();
          setChecked([checked, { value: false }]);
        })
        .catch((error) => {
          console.log("Error in load list:", error.response.status);
          setGroupUser(groupUser, { title: error.response.status });
        });
    };
    patchGoal(url, data);
    axios
      .patch(url, data, {
        headers: { Authorization: `Bearer ${JSON.parse(token)}` },
      })
      .then((response) => {
        loadGroup();
        setChecked([checked, { value: false }]);
      })
      .catch((error) => {
        console.log("Error in load list:", error.response.status);
        setGroupUser(groupUser, { title: error.response.status });
      });
  };

  return (
    <div className="habits-container">
      <h2>Metas do grupo</h2>
      <div>
        {groupUser &&
          groupUser.goals.map((elm, idx) => {
            return (
              <Card className="cardGoals" key={idx}>
                <h1 onClick={() => showGoalsDetails(elm)} className="cardTitle">
                  {" "}
                  {elm.title}
                </h1>
                <span className="flexHabits">
                  <div className="cardBoxHowMuch">
                    <div
                      className="cardHowMuch"
                      style={{ width: `${elm.how_much_achieved * 2}px` }}
                    ></div>
                  </div>
                  <MenuOpenOutlinedIcon
                    onClick={() => showGoalsDetails(elm)}
                    className="newIcon"
                    color="primary"
                  />
                  {!elm.achieved ? (
                    <Checkbox
                      checked={checked.value}
                      onChange={handleChange}
                      value={`${elm.id}`}
                      color="primary"
                    />
                  ) : (
                    <DoneAllOutlinedIcon color="primary" />
                  )}
                </span>
              </Card>
            );
          })}
        <Card className="newGoals">
          <span className="flexHabits">
            <h1 onClick={newGoals} className="newIcon">
              Adicionar meta
            </h1>
            <AddCircleRoundedIcon onClick={newGoals} className="newIcon" />
          </span>
        </Card>
        {goalsEdit && (
          <ModalCard open={openEdit} setOpen={setOpenEdit}>
            <GroupGoalsEdit
              elm={goalsEdit}
              setEditHabits={goalsEdit}
              setOpenEdit={setOpenEdit}
            />
          </ModalCard>
        )}
        {goalsCreate && (
          <ModalCard open={openNew} setOpen={setOpenNew}>
            <GroupGoalsCreate setOpenNew={setOpenNew} color="secondary" />
          </ModalCard>
        )}
      </div>
    </div>
  );
};

export default GroupGoalsList;
