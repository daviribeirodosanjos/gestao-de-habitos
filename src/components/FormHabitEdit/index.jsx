import { useState } from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import SaveIcon from "@material-ui/icons/Save";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
import { useToken } from "../../Providers/Token";
import { TextField } from "@material-ui/core";
import { useHabits } from "../../Providers/Habits";
import "./formHabitEdit.css";

const FormHabitEdit = ({ elm, setEditHabits, setOpenEdit }) => {
  const schema = yup.object().shape({
    title: yup.string().required("Campo obrigatório"),
    category: yup.string().required("Campo obrigatório"),
    difficulty: yup.string().required("Campo obrigatório"),
    frequency: yup.string().required("Campo obrigatório"),
    how_much_achieved: yup.number().required("Campo Obrigatório").integer(),
  });

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });

  const { token } = useToken();
  const { loadListHabits } = useHabits();

  const [input, setInput] = useState({
    title: elm.title,
    category: elm.category,
    difficulty: elm.difficulty,
    frequency: elm.frequency,
    achieved: elm.achieved,
    how_much_achieved: elm.how_much_achieved,
  });

  const patchHabit = (data) => {
    let bool_achieved = false;
    data.how_much_achieved < 100
      ? (bool_achieved = false)
      : (bool_achieved = true);
    data = { ...data, achieved: bool_achieved };

    const url = `https://kabit-api.herokuapp.com/habits/${elm.id}/`;
    axios
      .patch(url, data, {
        headers: { Authorization: `Bearer ${JSON.parse(token)}` },
      })
      .then((response) => {
        loadListHabits();
        setOpenEdit(false);
      })
      .catch((e) => console.log("Erro ao atualizar hábito:", e));
  };

  const deleteHabit = () => {
    const url = `https://kabit-api.herokuapp.com/habits/${elm.id}/`;
    axios
      .delete(url, {
        headers: { Authorization: `Bearer ${JSON.parse(token)}` },
      })
      .then((response) => {
        loadListHabits();
        setOpenEdit(false);
      })
      .catch((e) => console.log("Erro ao Deletar habito:", e));
  };

  const back = () => {
    setOpenEdit(false);
  };

  const handleChange = (e) => {
    switch (e.target.name) {
      case "title":
        setInput({ ...input, title: e.target.value });
        break;
      case "category":
        setInput({ ...input, category: e.target.value });
        break;
      case "frequency":
        setInput({ ...input, frequency: e.target.value });
        break;
      case "difficulty":
        setInput({ ...input, difficulty: e.target.value });
        break;
      case "achieved":
        setInput({ ...input, achieved: e.target.value });
        break;
      case "how_much_achieved":
        if(e.target.value > 100){
          e.target.value = 100;
        }
        if(e.target.value < 0){
          e.target.value = 0;
        }
        setInput({ ...input, how_much_achieved: Number(e.target.value) });
        break;
      default:
        break;
    }
  };

  return (
    <div className="habit-edit-container">
      <h2>Editar hábito</h2>
      <form onSubmit={handleSubmit(patchHabit)} >
        <TextField
          margin="normal"
          variant="outlined"
          label="Título"
          name="title"
          size="small"
          color="primary"
          value={input.title}
          inputRef={register}
          onChange={handleChange}
          error={!!errors.title}
          helperText={errors.title?.message}
        />
        <TextField
          margin="normal"
          variant="outlined"
          label="Categoria"
          name="category"
          size="small"
          color="primary"
          value={input.category}
          inputRef={register}
          onChange={handleChange}
          error={!!errors.category}
          helperText={errors.category?.message}
        />
        <TextField
          margin="normal"
          variant="outlined"
          label="Dificuldade"
          name="difficulty"
          size="small"
          color="primary"
          value={input.difficulty}
          inputRef={register}
          onChange={handleChange}
          error={!!errors.difficulty}
          helperText={errors.difficulty?.message}
        />
        <TextField
          margin="normal"
          variant="outlined"
          label="Frequência"
          name="frequency"
          size="small"
          color="primary"
          value={input.frequency}
          inputRef={register}
          onChange={handleChange}
          error={!!errors.frequency}
          helperText={errors.frequency?.message}
        />
        <TextField
          type="number"
          margin="normal"
          variant="outlined"
          label="Status"
          name="how_much_achieved"
          size="small"
          color="primary"
          value={input.how_much_achieved}
          inputRef={register}
          onChange={handleChange}
          error={!!errors.how_much_achieved}
          helperText={errors.how_much_achieved?.message}
        />
        <div className="habit-edit-buttons">
          <button type="submit" color="primary" onClick={()=>{}}>
              <SaveIcon fontSize="large" />
          </button>
          <button type='button' onClick={() => deleteHabit()}>
            <DeleteIcon fontSize="large" />
          </button>
          <button type='button' onClick={() => back()}>
            <ExitToAppIcon fontSize="large" />
          </button>
        </div>
       </form>     
     
    </div>
  );
};

export default FormHabitEdit;
