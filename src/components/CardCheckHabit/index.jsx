import { useHabits } from "../../Providers/Habits";
import { useState } from "react";
import { useToken } from "../../Providers/Token";
import FormHabitEdit from "../FormHabitEdit";
import FormHabitCreate from "../FormHabitCreate";
import { Card } from "@material-ui/core";
import Checkbox from "@material-ui/core/Checkbox";
import AddCircleRoundedIcon from "@material-ui/icons/AddCircleRounded";
import DoneAllOutlinedIcon from "@material-ui/icons/DoneAllOutlined";
import MenuOpenOutlinedIcon from "@material-ui/icons/MenuOpenOutlined";

import axios from "axios";

import "./cardCheckStyles.css";
import "../../styles/global";
import ModalCard from "../ModalCard";

const CardCheckHabit = () => {
  const { token } = useToken();
  const { habits, setHabits, loadListHabits } = useHabits();
  const [openEdit, setOpenEdit] = useState(false);
  const [openNew, setOpenNew] = useState(false);

  const [checked, setChecked] = useState([
    {
      value: false,
    },
  ]);

  const [habitEdit, setHabitEdit] = useState("");
  const [habitCreate, setHabitCreate] = useState(false);

  const handleChange = (event) => {
    setChecked([...checked, { value: true }]);
    const url = `https://kabit-api.herokuapp.com/habits/${event.target.value}/`;
    const data = { achieved: true, how_much_achieved: 100 };
    patchHabit(url, data);
  };

  const patchHabit = (url, data) => {
    axios
      .patch(url, data, {
        headers: { Authorization: `Bearer ${JSON.parse(token)}` },
      })
      .then((response) => {
        loadListHabits();
        setChecked([checked, { value: false }]);
      })
      .catch((error) => {
        console.log("Error in load list:", error.response.status);
        setHabits(habits, { title: error.response.status });
      });
  };

  const showHabitsDetails = (elm) => {
    setHabitEdit(elm);
    setOpenEdit(true);
  };

  const newHabits = () => {
    setOpenNew(true);
    setHabitCreate(true);
  };

  return (
    <div className="habits-container">
      <h2>Seus hábitos</h2>
      <div>
        {habits &&
          [...habits].map((elm, idx) => (
            <Card className="cardHabits" key={idx}>
              <h1 onClick={() => showHabitsDetails(elm)} className="cardTitle">
                {" "}
                {elm.title}
              </h1>
              <span className="flexHabits">
                <div className="cardBoxHowMuch">
                  <div
                    className="cardHowMuch"
                    style={{ width: `${elm.how_much_achieved * 2}px` }}
                  ></div>
                </div>
                <MenuOpenOutlinedIcon
                  onClick={() => showHabitsDetails(elm)}
                  className="newIcon"
                  color="primary"
                />
                {!elm.achieved ? (
                  <Checkbox
                    checked={checked.value}
                    onChange={handleChange}
                    value={`${elm.id}`}
                    color="primary"
                  />
                ) : (
                  <DoneAllOutlinedIcon color="primary" />
                )}
              </span>
            </Card>
          ))}
        <Card className="newHabits">
          <span className="flexHabits">
            <h1 onClick={newHabits} className="newIcon">
              Adicionar hábito
            </h1>
            <AddCircleRoundedIcon onClick={newHabits} className="newIcon" />
          </span>
        </Card>
        {habitEdit && (
          <ModalCard open={openEdit} setOpen={setOpenEdit}>
            <FormHabitEdit
              elm={habitEdit}
              setEditHabits={setHabitEdit}
              setOpenEdit={setOpenEdit}
            />
          </ModalCard>
        )}
        {habitCreate && (
          <ModalCard open={openNew} setOpen={setOpenNew}>
            <FormHabitCreate
              setHabitsCreate={setHabitCreate}
              setOpenNew={setOpenNew}
              color="secondary"
            />
          </ModalCard>
        )}
      </div>
    </div>
  );
};

export default CardCheckHabit;
