import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useState } from "react";
import axios from "axios";
import { useToken } from "../../Providers/Token";
import { useUser } from "../../Providers/User";
import { createMuiTheme, styled } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import MuiAlert from "@material-ui/lab/Alert";
import { TextField } from "@material-ui/core";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import SaveIcon from "@material-ui/icons/Save";
import "./useEditStyle.css";

const Text = styled(TextField)({
  margin: "5px",
  width: "350px",
});

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const UserEdit = ({ setOpen }) => {
  const { token } = useToken();
  const { user, setUser } = useUser();

  const [error, setError] = useState(false);

  const [input, setInput] = useState({
    username: user.username,
    email: user.email,
  });

  const schema = yup.object().shape({
    email: yup.string().email("Email inválido").required("Campo obrigatório"),
    username: yup.string().required("Campo obrigatório"),
  });

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(schema),
  });

  const handleForm = (data) => {
    const url = `https://kabit-api.herokuapp.com/users/${user.id}/`;

    axios
      .patch(url, data, {
        headers: { Authorization: `Bearer ${JSON.parse(token)}` },
      })
      .then((response) => {
        setUser(response.data);
        back();
      })
      .catch((error) => {
        if (error.response.status === 400) {
          setError("Este nome de usuário já existe!");
        }
      });
  };

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: "#6325a0",
      },
      secondary: {
        main: "#3BA597",
      },
      error: {
        main: "#FF220C",
      },
    },
  });

  const handleChange = (e) => {
    switch (e.target.name) {
      case "username":
        setInput({ ...input, username: e.target.value });
        break;
      case "email":
        setInput({ ...input, email: e.target.value });
        break;
      default:
        break;
    }
  };

  const back = () => {
    setOpen(false);
  };

  return (
    <div className="user-edit-container">
      <h1>Editar Usuário</h1>
      <br />
      <form onSubmit={handleSubmit(handleForm)}>
        <ThemeProvider theme={theme}>
          <Text
            autoComplete="username"
            margin="normal"
            variant="outlined"
            label="Username"
            name="username"
            size="small"
            color="primary"
            value={input.username}
            onChange={handleChange}
            inputRef={register}
            error={!!errors.username}
            helperText={errors.username?.message}
          />
          <Text
            margin="normal"
            variant="outlined"
            label="Email"
            name="email"
            size="small"
            color="primary"
            value={input.email}
            onChange={handleChange}
            inputRef={register}
            error={!!errors.email}
            helperText={errors.email?.message}
          />
          <div className="buttons-edit-user">
            <button type="submit">
              <SaveIcon fontSize="large" />
            </button>
            <button onClick={() => back()}>
              <ExitToAppIcon fontSize="large" />
            </button>
          </div>
        </ThemeProvider>
      </form>
      {error && (
        <Alert severity="warning" className="alert">
          {error}
        </Alert>
      )}
    </div>
  );
};
export default UserEdit;
