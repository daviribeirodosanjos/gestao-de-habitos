import { createContext, useContext, useEffect, useState } from "react";
import axios from "axios";
import {useToken} from '../Token';
// import {useUpdate} from '../Update';

const HabitsContext = createContext();

export const HabitsProvider = ({ children }) => {
  const [habits, setHabits] = useState();
  const {token} = useToken() || '';
  // const {update} = useUpdate();

  useEffect(()=> {    
    token && loadListHabits();    
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[token]);
  

  const loadListHabits = () => {    
    axios
      .get("https://kabit-api.herokuapp.com/habits/personal/",{
      headers:{'Authorization': `Bearer ${JSON.parse(token)}`}})
      .then((response) => {        
        setHabits(response.data);        
      })
      .catch((error) => {        
          console.log('Error in load list:', error.response.status)
          setHabits(habits, {title:error.response.status})
      });      
  };

  return (
    <HabitsContext.Provider value={{ habits, setHabits,loadListHabits}}>
      {children}
    </HabitsContext.Provider>
  );
};

export const useHabits = () => useContext(HabitsContext);
