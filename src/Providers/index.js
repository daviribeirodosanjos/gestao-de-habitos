import { TokenProvider } from "./Token/index";
import { UserProvider } from "./User/index";
import { GroupsProvider } from "./Groups/index";
import { HabitsProvider } from "./Habits";

const Providers = ({ children }) => {
  return (
    <TokenProvider>
      <UserProvider>
        <GroupsProvider>
          <HabitsProvider>{children}</HabitsProvider>
        </GroupsProvider>
      </UserProvider>
    </TokenProvider>
  );
};

export default Providers;
