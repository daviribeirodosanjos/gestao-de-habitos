import { useState } from "react";
import "./group.css";
import Header from "../../components/Header";
import ListGroups from "../../components/ListGroups";
import MenuMobile from "../../components/MenuMobile";
import CreateGroup from "../../components/CreateGroup";
import ModalCard from "../../components/ModalCard";

const Group = () => {
  const [, setActivitiesCreate] = useState(false);
  const [openNew, setOpenNew] = useState(false);

  const newActivities = () => {
    setOpenNew(true);
    setActivitiesCreate(true);
  };

  return (
    <div className="page-groups">
      <Header />
      <button onClick={newActivities}>Criar um Grupo</button>
      <ModalCard open={openNew} setOpen={setOpenNew}>
        <CreateGroup setOpenNew={setOpenNew} color="secondary" />
      </ModalCard>
      <ListGroups />
      <MenuMobile />
    </div>
  );
};

export default Group;
