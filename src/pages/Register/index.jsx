import FormRegister from "../../components/FormRegister";
import Header from "../../components/Header";
const Register = () => {
  return (
    <div>
      <Header />
      <FormRegister />
    </div>
  );
};

export default Register;
