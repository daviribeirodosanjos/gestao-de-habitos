import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`


html{

  padding: 4em 0;

} 
html, body {
    background: #f5f5f5;
    color: #312e38;
    -webkit-font-smoothing: antialiased !important;
    width: 100%;
    font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  }

  body, input, button {  
    font-size: 1.5rem;
  }  

  h1, h2, h3, h4, h5, h6, strong {
    font-weight: 600;
  }

  button {
    margin: 8px 8px;
    box-sizing: border-box;
    min-width: 80px;
    line-height: 1.75;
    font-size: 14pt;
    font-weight: 400;
    border-radius: 8px;
    color: #fff;
    background-color: #284b63;
    cursor: pointer;
  }
  
  button:hover {   
    -webkit-transform: scale(1.1);
    -ms-transform: scale(1.1);
    transform: scale(1.1);
  }


  a {
    text-decoration: none;
  }

  .tooltip {
    display:none;
    position: relative;
    font-weight: bold;
  }

  .tooltip:after {
    display: none;
    position: absolute;
    top: -5px;
    padding: 5px;
    border-radius: 3px;
    left: calc(100% + 2px);
    content: attr(data-tooltip);
    white-space: nowrap;
    background-color: #0095ff;
    color: White;
  }

  .tooltip:hover:after {
    display: block;
  }




`;
